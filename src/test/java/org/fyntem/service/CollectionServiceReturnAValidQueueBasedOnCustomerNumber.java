package org.fyntem.service;

import org.fyntem.entity.Customer;
import org.fyntem.entity.DayOfWeek;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionServiceReturnAValidQueueBasedOnCustomerNumber {

    @Test
    public void testReturnAValidQueueBasedOnCustomerNumber1() {
        List<Customer> customers = List.of(
                new Customer(6, "Mike"),
                new Customer(3, "John"),
                new Customer(1, "Garry"),
                new Customer(7, "Ian"),
                new Customer(2, "Andy"),
                new Customer(4, "Liz"),
                new Customer(5, "Fred")
        );

        Queue<Customer> expectedQueue = new ArrayDeque<>();
        expectedQueue.add(new Customer(1, "Garry"));
        expectedQueue.add(new Customer(2, "Andy"));
        expectedQueue.add(new Customer(3, "John"));
        expectedQueue.add(new Customer(4, "Liz"));
        expectedQueue.add(new Customer(5, "Fred"));

        Queue<Customer> actualQueue = CollectionService.returnAValidQueueBasedOnCustomerNumber(customers, DayOfWeek.MONDAY);

        assertIterableEquals(expectedQueue, actualQueue);
    }

    @Test
    public void testReturnAValidQueueBasedOnCustomerNumber2() {
        List<Customer> customers = List.of(
                new Customer(6, "Mike"),
                new Customer(3, "John"),
                new Customer(1, "Garry"),
                new Customer(7, "Ian"),
                new Customer(2, "Andy"),
                new Customer(4, "Liz"),
                new Customer(5, "Fred")
        );

        Queue<Customer> expectedQueue = new ArrayDeque<>();
        expectedQueue.add(new Customer(1, "Garry"));
        expectedQueue.add(new Customer(2, "Andy"));
        expectedQueue.add(new Customer(3, "John"));
        expectedQueue.add(new Customer(4, "Liz"));
        expectedQueue.add(new Customer(5, "Fred"));
        expectedQueue.add(new Customer(6, "Mike"));
        expectedQueue.add(new Customer(7, "Ian"));

        Queue<Customer> actualQueue = CollectionService.returnAValidQueueBasedOnCustomerNumber(customers, DayOfWeek.WEDNESDAY);

        assertIterableEquals(expectedQueue, actualQueue);
    }

    @Test
    public void testReturnAValidQueueBasedOnCustomerNumber3() {
        List<Customer> customers = List.of(
                new Customer(6, "Mike"),
                new Customer(3, "John"),
                new Customer(1, "Garry"),
                new Customer(7, "Ian"),
                new Customer(2, "Andy"),
                new Customer(4, "Liz"),
                new Customer(5, "Fred")
        );

        assertThrows(IllegalArgumentException.class, () -> CollectionService.returnAValidQueueBasedOnCustomerNumber(customers, DayOfWeek.SUNDAY), "No customers are expected on SUNDAY");
    }

}
