package org.fyntem.service;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CollectionServicePutNElementsToTheBeginning {

    @Test
    public void testPutNElementsToTheBeginning1() {
        List<String> wordList = List.of("one", "two", "three", "four", "five");
        List<String> resultList = CollectionService.putNElementsToTheBeginning(wordList, 3);

        List<String> expectedList = new LinkedList<>();
        expectedList.add("four");
        expectedList.add("five");
        expectedList.add("one");
        expectedList.add("two");
        expectedList.add("three");
        assertLinesMatch(expectedList, resultList);
    }

    @Test
    public void testPutNElementsToTheBeginning2() {
        List<String> wordList = List.of("one", "two", "three", "four", "five");
        List<String> resultList = CollectionService.putNElementsToTheBeginning(wordList, 2);

        List<String> expectedList = new LinkedList<>();
        expectedList.add("three");
        expectedList.add("four");
        expectedList.add("five");
        expectedList.add("one");
        expectedList.add("two");
        assertLinesMatch(expectedList, resultList);
    }

    @Test
    public void testPutNElementsToTheBeginning3() {
        List<String> wordList = List.of("one", "two", "three", "four", "five");
        List<String> resultList = CollectionService.putNElementsToTheBeginning(wordList, 0);

        List<String> expectedList = new LinkedList<>();
        expectedList.add("one");
        expectedList.add("two");
        expectedList.add("three");
        expectedList.add("four");
        expectedList.add("five");
        assertLinesMatch(expectedList, resultList);
    }

    @Test
    public void testPutNElementsToTheBeginning4() {
        List<String> wordList = List.of("one", "two", "three", "four", "five");

        assertThrows(IllegalArgumentException.class, () -> CollectionService.putNElementsToTheBeginning(wordList, -1), "N must be non-negative");
    }

}
