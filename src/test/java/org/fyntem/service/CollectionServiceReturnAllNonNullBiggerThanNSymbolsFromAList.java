package org.fyntem.service;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionServiceReturnAllNonNullBiggerThanNSymbolsFromAList {

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList1() {
        List<String> wordList = List.of("The", "", "brand", "       ", "new", "experience");
        List<String> resultArray = CollectionService.returnAllNonNullBiggerThanNSymbolsFromAList(wordList, 3);

        assertLinesMatch(List.of("The", "new"), resultArray);
    }

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList2() {
        List<String> wordList = new ArrayList<>();
        wordList.add("The");
        wordList.add(null);
        wordList.add("brand");
        wordList.add("       ");
        wordList.add("new");
        wordList.add("experience");

        List<String> resultArray = CollectionService.returnAllNonNullBiggerThanNSymbolsFromAList(wordList, 7);

        assertLinesMatch(List.of("The", "brand", "new"), resultArray);
    }

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList3() {
        List<String> wordList = List.of("The", "", "brand", "       ", "new", "experience");
        List<String> resultArray = CollectionService.returnAllNonNullBiggerThanNSymbolsFromAList(wordList, 0);

        assertLinesMatch(List.of(""), resultArray);
    }

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList4() {
        List<String> wordList = List.of("The", "", "brand", "       ", "new", "experience");

        assertThrows(IllegalArgumentException.class, () -> CollectionService.returnAllNonNullBiggerThanNSymbolsFromAList(wordList, -1), "Limit must be non-negative");
    }

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList5() {
        List<String> wordList = List.of("The", "", "brand", "       ", "new", "experience");
        List<String> resultArray = CollectionService.returnAllNonNullBiggerThanNSymbolsFromAListLambda(wordList, 3);

        assertLinesMatch(List.of("The", "new"), resultArray);
    }

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList6() {
        List<String> wordList = new ArrayList<>();
        wordList.add("The");
        wordList.add(null);
        wordList.add("brand");
        wordList.add("       ");
        wordList.add("new");
        wordList.add("experience");

        List<String> resultArray = CollectionService.returnAllNonNullBiggerThanNSymbolsFromAListLambda(wordList, 7);

        assertLinesMatch(List.of("The", "brand", "new"), resultArray);
    }

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList7() {
        List<String> wordList = List.of("The", "", "brand", "       ", "new", "experience");
        List<String> resultArray = CollectionService.returnAllNonNullBiggerThanNSymbolsFromAListLambda(wordList, 0);

        assertLinesMatch(List.of(""), resultArray);
    }

    @Test
    public void testReturnAllNonNullBiggerThanNSymbolsFromAList8() {
        List<String> wordList = List.of("The", "", "brand", "       ", "new", "experience");

        assertThrows(IllegalArgumentException.class, () -> CollectionService.returnAllNonNullBiggerThanNSymbolsFromAListLambda(wordList, -1), "Limit must be non-negative");
    }

}
