package org.fyntem.service;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionServiceReturnAllNonNullDuplicationsFromAList {

    @Test
    public void testReturnReturnAllNonNullDuplicationsFromAList1() {
        List<String> wordList = List.of("Sometimes", "we", "use", "", "the", "time", "the", "", "different", "way", "we", "expect");
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueWordsFromAList(wordList);

        assertIterableEquals(new LinkedHashSet<>(List.of("Sometimes", "we", "use", "the", "time", "different", "way", "expect")), resultSet);
    }

    @Test
    public void testReturnReturnAllNonNullDuplicationsFromAList2() {
        List<String> wordList = new ArrayList<>();
        wordList.add("");
        wordList.add(null);
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueWordsFromAList(wordList);

        assertNull(resultSet);
    }

    @Test
    public void testReturnReturnAllNonNullDuplicationsFromAList3() {
        List<String> wordList = List.of("Sometimes", "we", "use", "", "the", "time", "the", "", "different", "way", "we", "expect");
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueWordsFromAListLambda(wordList);

        assertIterableEquals(new LinkedHashSet<>(List.of("Sometimes", "we", "use", "the", "time", "different", "way", "expect")), resultSet);
    }

    @Test
    public void testReturnReturnAllNonNullDuplicationsFromAList4() {
        List<String> wordList = new ArrayList<>();
        wordList.add("");
        wordList.add(null);
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueWordsFromAListLambda(wordList);

        assertNull(resultSet);
    }

}
