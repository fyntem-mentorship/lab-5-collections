package org.fyntem.entity;

public enum DayOfWeek {

    MONDAY(5),
    TUESDAY(6),
    WEDNESDAY(7),
    THURSDAY(7),
    FRIDAY(3),
    SATURDAY(0),
    SUNDAY(0);

    private final int queueLimit;

    DayOfWeek(int queueLimit) {
        this.queueLimit = queueLimit;
    }

    public int getQueueLimit() {
        return queueLimit;
    }
}
