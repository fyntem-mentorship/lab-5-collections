package org.fyntem.entity;

public class University {
    private String name;
    private GradeLevel lowerGradeAcceptingFrom;

    public University(String name, GradeLevel lowerGradeAcceptingFrom) {
        this.name = name;
        this.lowerGradeAcceptingFrom = lowerGradeAcceptingFrom;
    }

    public String getName() {
        return name;
    }

    public GradeLevel getLowerGradeAcceptingFrom() {
        return lowerGradeAcceptingFrom;
    }
}
