package org.fyntem.entity;

public class Customer {
    private int queueNumber;
    private String customerName;

    public Customer(int queueNumber, String customerName) {
        this.queueNumber = queueNumber;
        this.customerName = customerName;
    }

    public int getQueueNumber() {
        return queueNumber;
    }

    public String getCustomerName() {
        return customerName;
    }
}
