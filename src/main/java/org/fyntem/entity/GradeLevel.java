package org.fyntem.entity;

public enum GradeLevel {

    A(95),
    B(82),
    C(75),
    D(68),
    E(60),
    F(0);

    private final int gradeStartsFrom;

    GradeLevel(int gradeStartsFrom) {
        this.gradeStartsFrom = gradeStartsFrom;
    }

    public int getGradeStartsFrom() {
        return gradeStartsFrom;
    }
}
