package org.fyntem.entity;

public class Student {
    private String surname;
    private int averageGrade; // range [0 - 100]

    public Student(String surname, int averageGrade) {
        this.surname = surname;
        this.averageGrade = averageGrade;
    }

    public String getSurname() {
        return surname;
    }

    public int getAverageGrade() {
        return averageGrade;
    }
}
