package org.fyntem.service;

import org.fyntem.entity.Student;
import org.fyntem.entity.University;

import java.util.List;
import java.util.Map;

public class MapService {

    /**
     * Given the List of Students and Universities.
     *
     * @return mapping of University name to Students that are allowed to enter that University.
     *
     * Requirements:
     * 1. be able to guess, why the University name, instead of plain University object were used in returning map;
     * 2. explain how a HashMap works.
     */
    public static Map<String, List<Student>> returnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule(List<Student> students, List<University> universities) {
        return null;
    }

}
