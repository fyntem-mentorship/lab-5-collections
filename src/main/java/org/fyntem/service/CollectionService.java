package org.fyntem.service;

import org.fyntem.entity.Customer;
import org.fyntem.entity.DayOfWeek;

import java.util.List;
import java.util.Queue;
import java.util.Set;

public class CollectionService {

    /**
     * Given a List of elements, N - symbolsLimit
     * Using for-cycle
     * @return all non-null/non-empty words that have equal or less than N symbols.
     */
    public static List<String> returnAllNonNullBiggerThanNSymbolsFromAList(List<String> stringList, int symbolsLimit) {
        return null;
    }

    /**
     * Given a List of elements, N - symbolsLimit
     * Using lambda-expressions
     * @return all non-null/non-empty words that have less than N symbols.
     */
    public static List<String> returnAllNonNullBiggerThanNSymbolsFromAListLambda(List<String> stringList, int symbolsLimit) {
        return null;
    }

    /**
     * Given a List of elements and a number N.
     * Take N elements from the end of a list and append them to it's beginning.
     *
     * Example: ["one", "two", "three", "four", "five"], N = 3
     * return ["three", "four", "five", "one", "two"]
     *
     * @return new list.
     */
    public static List<String> putNElementsToTheBeginning(List<String> stringList, int n) {
        return null;
    }

    /**
     * Given a List of Customers and a day of week.
     *
     * @return queue of the names of all Customers based on their queue number and
     * add restrict them to the operational day
     *
     * Example: [{"Mike", 6}, {"John", 3}, {"Garry", 1}, {"Ian", 7}, {"Andy", 2}, {"Liz", 4}, {"Fred", 5}], DayOfWeek = MONDAY
     * return [{"Garry", 1}, {"Andy", 2}, {"John", 3}, {"Liz", 4}, {"Fred", 5}]
     *
     * Requirement:
     * Select the most fit Queue Interface implementation and explain why you did so.
     * Also be ready to explain: "can we use something better than queue here than a queue? If so - in which cases?"
     */
    public static Queue<Customer> returnAValidQueueBasedOnCustomerNumber(List<Customer> stringList, DayOfWeek dayOfWeek) {
        return null;
    }

    /**
     * Given a List of elements.
     * Using for-cycle
     * @return all non-null/non-empty unique words from a given list.
     */
    public static Set<String> returnAllNonNullUniqueWordsFromAList(List<String> stringList) {
        return null;
    }

    /**
     * Given a List of elements.
     * Using lambda-expressions
     *
     * @return all non-null/non-empty unique words from a given list.
     */
    public static Set<String> returnAllNonNullUniqueWordsFromAListLambda(List<String> stringList) {
        return null;
    }

}
